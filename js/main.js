$(document).ready(function() {

    (() => {
        'use strict'

        // Checking to see if the browser supports indexeDB
        if(!('indexedDB' in window)) {
            console.warn('IndexedDB not supported');
            return;
        }

        const name = 'repositories';
        const name1 = 'names';

        idb.delete(name).then( () => console.log('Done, erased'));

        const dbPromise = idb.open(name, 1, upgradeDB => {
            console.log('Making a new object store');
            if(!upgradeDB.objectStoreNames.contains(name1)) {
                let repos = upgradeDB.createObjectStore(name1, { KeyPath: 'created', autoIncrement: true });
            }
        });
    })();

    $("#searchUser").keypress(function(e) {
        if(e.which == 13) {
            $(".row").empty();  // Clean cards case new user is searched
            $('#btn-language').attr('value', 'none');
            $('#btn-language').text('Filter by language ');
            $('#user').empty();
            $('.dropdown-menu').empty();
            if($("#searchUser").val() != "") searchUser();
        }
    });

    $(document).on('click', '.dropdown-item', function(){
        $('.row').empty();
        $('#btn-language').html($(this).attr('value'));
        languageFilter($(this).attr('value'));
    });

});

// Gets the name of the user on input
const getUser = () => $('#searchUser').val();

// Function to return every starred project from the user
const searchUser = () => {
    const name = 'repositories';
    const name1 = 'names';

    const dbPromise = idb.open(name, 1);

    $.ajax({
        url: 'https://api.github.com/users/'+getUser()
    }).done( user => buildUser(user) );

    arr = [];
    $.ajax({
        url: "https://api.github.com/users/"+getUser()+"/repos"
    }).done( repos => {
        dbPromise.then( db => {
            let tx = db.transaction(name1, 'readwrite');
            let store = tx.objectStore(name1);
            $(repos).each(function(index){
                buildCards(repos[index]);
                if($.inArray(repos[index].language, arr) == -1) {
                    arr.push(repos[index].language);
                }
                store.put(repos[index]);
            });
            return store.getAll();
        }).then( val => {
            arr.push('none');
            $(arr).each(function(index){
                $('#dropdownLanguages').append(
                    '<a class="dropdown-item language" value="' + arr[index] + '">' + arr[index] + '</a>'
                );
            });
            console.log('Usuário arquivado');
        });

    });
};

// Filter the repos languages
const languageFilter = language => {
    const name = 'repositories';
    const name1 = 'names';

    const dbPromise = idb.open(name, 1);

    dbPromise.then( db => {
        let tx = db.transaction(name1, 'readwrite');
        let store = tx.objectStore(name1);
        return store.getAll();
    }).then( val => {
        if(language !== 'none') {
            let arr = [];

            arr = $.grep(val, function(value){
                return (value.language === language);
            });

            $(arr).each(function(index){
                buildCards(arr[index]);
            });
        } else {
            $(val).each( index => {
                buildCards(val[index]);
            });
        }
        return;
    }).then( () => {
        console.log('Done');
    })
}

// Create the user card info
const buildUser = info => {
    $('#user').append(
        '<div class="media border rounded">' +
            '<img class="mr-3 img-thumbnail" src="' + info.avatar_url + '" id="userImage">' +
            '<div class="media-body card-body">' +
                '<h2 class="mt-0">' +
                    '<strong>' + info.name + '</strong>' +
                '</h2>' +
                '<small class="text-muted">' + info.login + '</small>' +
                '<p>' + info.bio + '</p>' +
            '</div>' +
        '</div>'
    );
}

// Function to create the cards
function buildCards(repos) {
    $("#cards").append("<div class='col-md-4' id='" + repos.name + " " + repos.language + "'>"
                    +  "<div class='card' style='width: 18rem;'>"
                    +  "<div class='card-body'>"
                    +  "<h3 class='card-title'><b>" + repos.name + "</b></h3>"
                    +  "<p class='card-text'>" + repos.description + "</p>"
                    +  "<p>"
                    +  "<div class='d-inline octicon octicon-star badge badge-pill badge-danger'> " + repos.stargazers_count + "</div>  "
                    +  "<div class='d-inline octicon octicon-issue-opened badge badge-pill badge-primary'> " + repos.open_issues + "</div>"
                    +  "</p>"
                    +  "<div class='d-flex justify-content-between align-items-center'>"
                    +  "<div class='btn-group'>"
                            + "<a href='" + repos.html_url + "'><button type='button' class='btn btn-sm btn-outline-secondary'>View</button></a>"
                    +  "</div>"
                    +  "<small class='text-muted'>" + repos.language + "</small>"
                    +    "</div>"
                    +"</div></div></div>"
    );
};
